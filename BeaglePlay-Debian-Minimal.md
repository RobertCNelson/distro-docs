# BeaglePlay Debian 12.6 2024-07-04 Minimal

```
Debian Minimal (non-graphical) image for <a href="/boards/beagleplay">BeaglePlay</a> based on TI AM62 processor

Remember to set your user password in the sysconf.txt file located in the fat32 partition before you boot (or use bb-imager and set it in the gui).

<ul>
 	<li>Kernel: <a href="https://github.com/beagleboard/linux/tree/6.6.32-ti-arm64-r7">6.6.32-ti-arm64-r7</a></li>
 	<li>U-Boot: <a href="https://openbeagle.org/beagleboard/u-boot/-/tree/v2024.07-rc5-BeaglePlay">v2024.07-rc5-BeaglePlay</a></li>
</ul>

Board Information: <a href="https://www.beagleboard.org/boards/beagleplay">BeaglePlay</a>
Board Documentation: <a href="https://docs.beagle.cc/latest/boards/beagleplay/02-quick-start.html">quick-start</a>
```

```
https://rcn-ee.net/rootfs/debian-arm64-12-bookworm-minimal-v6.6-ti/2024-07-04/beagleplay-debian-12.6-minimal-arm64-2024-07-04-8gb.img.xz
```

# BeaglePlay Debian 12.6 2024-07-04 Minimal Flasher

```
Debian eMMC Flasher Minimal (non-graphical) image for <a href="/boards/beagleplay">BeaglePlay</a> based on TI AM62 processor

Remember to set your user password in the sysconf.txt file located in the fat32 partition before you boot (or use bb-imager and set it in the gui).

<ul>
 	<li>Kernel: <a href="https://github.com/beagleboard/linux/tree/6.6.32-ti-arm64-r7">6.6.32-ti-arm64-r7</a></li>
 	<li>U-Boot: <a href="https://openbeagle.org/beagleboard/u-boot/-/tree/v2024.07-rc5-BeaglePlay">v2024.07-rc5-BeaglePlay</a></li>
</ul>

Board Information: <a href="https://www.beagleboard.org/boards/beagleplay">BeaglePlay</a>
Board Documentation: <a href="https://docs.beagle.cc/latest/boards/beagleplay/02-quick-start.html">quick-start</a>
```

```
https://rcn-ee.net/rootfs/debian-arm64-12-bookworm-minimal-v6.6-ti/2024-07-04/beagleplay-emmc-flasher-debian-12.6-minimal-arm64-2024-07-04-8gb.img.xz
```
