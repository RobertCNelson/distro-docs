# BBAI64 Debian 12.6 2024-07-04 XFCE

```
Debian graphical user interface (Xfce) image for <a href="/boards/beaglebone-ai-64">BeagleBone AI-64</a> based on TI TDA4VM processor

Remember to set your user password in the sysconf.txt file located in the fat32 partition before you boot (or use bb-imager and set it in the gui).

<ul>
 	<li>Kernel: <a href="https://github.com/beagleboard/linux/tree/v6.1.83-ti-arm64-r61">6.1.83-ti-arm64-r61</a></li>
 	<li>U-Boot: <a href="https://github.com/u-boot/u-boot/releases/tag/v2024.07-rc5">v2024.07-rc5</a></li>
 	<li>VSCode: <a href="https://github.com/coder/code-server/releases/tag/v4.90.3">v4.90.3</a></li>
 	<li>Node-RED: <a href="https://github.com/node-red/node-red/releases/tag/4.0.2">v4.0.2</a></li>
</ul>

Board Information: <a href="https://www.beagleboard.org/boards/beaglebone-ai-64">BeagleBone AI-64</a>
Board Documentation: <a href="https://docs.beagle.cc/latest/boards/beaglebone/ai-64/02-quick-start.html">quick-start</a>
```

```
https://rcn-ee.net/rootfs/debian-arm64-12-bookworm-xfce-v6.1-ti/2024-07-04/bbai64-debian-12.6-xfce-arm64-2024-07-04-12gb.img.xz
```

# BBAI64 Debian 12.6 2024-07-04 XFCE Flasher

```
Debian eMMC Flasher graphical user interface (Xfce) image for <a href="/boards/beaglebone-ai-64">BeagleBone AI-64</a> based on TI TDA4VM processor

Remember to set your user password in the sysconf.txt file located in the fat32 partition before you boot (or use bb-imager and set it in the gui).

<ul>
 	<li>Kernel: <a href="https://github.com/beagleboard/linux/tree/v6.1.83-ti-arm64-r61">6.1.83-ti-arm64-r61</a></li>
 	<li>U-Boot: <a href="https://github.com/u-boot/u-boot/releases/tag/v2024.07-rc5">v2024.07-rc5</a></li>
 	<li>VSCode: <a href="https://github.com/coder/code-server/releases/tag/v4.90.3">v4.90.3</a></li>
 	<li>Node-RED: <a href="https://github.com/node-red/node-red/releases/tag/4.0.2">v4.0.2</a></li>
</ul>

Board Information: <a href="https://www.beagleboard.org/boards/beaglebone-ai-64">BeagleBone AI-64</a>
Board Documentation: <a href="https://docs.beagle.cc/latest/boards/beaglebone/ai-64/02-quick-start.html">quick-start</a>
```

```
https://rcn-ee.net/rootfs/debian-arm64-12-bookworm-xfce-v6.1-ti/2024-07-04/bbai64-emmc-flasher-debian-12.6-xfce-arm64-2024-07-04-12gb.img.xz
```
